FROM nginx:latest

RUN rm /etc/nginx/conf.d/default.conf
COPY production.development.conf /etc/nginx/conf.d/default.conf
COPY dev_certs/ /etc/nginx/ssl/

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
